from django.conf.urls import include, url
from django.contrib import admin
from store import views

from . import views # import views so we can use them in urls.


# urlpatterns = [
#     url(r'^$', views.index), # "/store" will call the method "index" in "views.py"
# ]

...
urlpatterns = [
    #url(r'^$', views.index),
    url(r'^$', views.listing, name='listing'),
    url(r'^(?P<album_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^search/$', views.search, name='search'),
]