from django.db import models

""" ARTISTS = {
  'mickael-jackson': {'name': 'Mickael Jackson'},
  'lej': {'name': 'Elijay'},
  #'rosana': {'name': 'Rosana'},
  'bob-dylan': {'name': 'Bob Dylan'},
}


ALBUMS = [
  {'name': 'Bad', 'artists': [ARTISTS['mickael-jackson']]},
  {'name': 'La Dalle', 'artists': [ARTISTS['lej']]},
  #{'name': 'Luna Nueva', 'artists': [ARTISTS['rosana'], ARTISTS['maria-dolores-pradera']]}
  {'name': 'The Freewheelin', 'artists': [ARTISTS['bob-dylan']]},
] """


class Artist(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Contact(models.Model):
    email = models.EmailField(max_length=100)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Album(models.Model):
    reference = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(default=True)
    title = models.CharField(max_length=200)
    picture = models.TextField()
    artists = models.ManyToManyField(Artist, related_name='albums', blank=True)

    def __str__(self):
        return self.title

class Booking(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    contacted = models.BooleanField(default=False)
    album = models.OneToOneField(Album, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    def __str__(self):
        return self.contact.name